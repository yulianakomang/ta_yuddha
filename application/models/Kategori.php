<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Model {

	protected $table = 'tb_kategori';

	public function fetchAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->result();
    }

    public function get_where($data = '')
    {
        $this->db->where($data);
        return $this->db->get($this->table)->result();
    }

    public function insert($input = '')
    {
        $data = array(
            'nama_kategori'         => $input->nama_kategori,
            'deskripsi'         => $input->deskripsi,
            
            );
        $this->db->insert($this->table, $data);
    }

    public function update($input = '',$where)
    {
        
        $data = array(
        'nama_kategori'         => $input->nama_kategori,
        'deskripsi'         => $input->deskripsi,

        );
       
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function delete($where)
    {
        $this->db->delete($this->table,$where);
    }

}