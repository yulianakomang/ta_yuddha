<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb_area bg-img" style="background-image: url(http://localhost/project/ta_vinokha/public/front/img/bg-img/breadcumb.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="page-title text-center">
                    <h2>Keranjang</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Checkout Area Start ##### -->
<div class="checkout_area section-padding-80">
    <div class="container">
                <div class="row">

            <div class="col-12 col-md-6">
                 
                <div class="cart-page-heading mb-30">
                    <h5>Keranjang</h5>
                </div>
                
                <?php if($this->session->userdata('cart') != NULL){ foreach ($this->session->userdata('cart') as $key=>$item): 

                ?>
                
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="<?= $item['img'] ?>" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <span type="button" class="close mr-2 mt-1 clear_cart" data-id="<?= $key ?>" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </span>
                            <div class="card-body">
                                
                                <a href="<?= base_url('detail/'.$item['code']) ?>">
                                    <h6><?= $item['title'] ?></h6>
                                </a>
                                <p class="product-price"><?= $fx->_rupiah($item['harga']) ?>
                                    <br>

                                <?php 
                                $data_produk = $db_produk->get_where(array('id'=>$item['id']));
                                $stok = $data_produk[0]->stok;
                                $future_stok= ($item['total']+1);
                                 ?>


                                    <button class="btn btn-default min-to-cart" data-id="<?= $item['id'] ?>" data-code="<?= $item['code'] ?>" data-title="<?= $item['title'] ?>" data-img="<?= $item['img'] ?>" data-harga="<?= $item['harga'] ?>">-</button>
                                    <input type="text" class="form-control" readonly style="width: 50px;display: inline;" value="<?= $item['total'] ?>">
                                    <?php  if ($future_stok <= $stok): ?>
                                    <button class="btn btn-default add-to-cart" data-id="<?= $item['id'] ?>" data-code="<?= $item['code'] ?>" data-title="<?= $item['title'] ?>" data-img="<?= $item['img'] ?>" data-harga="<?= $item['harga'] ?>">+</button>
                                    <?php else: ?>
                                        Stok Habis
                                    <?php endif ?>
                                   
                            </div>
                        </div>
                    </div>
                </div>

                <?php endforeach; }else{ echo "Tidak Ada Barang."; } ?>

                <?php if ($this->session->flashdata('sendorder')): ?>
                    <div class="alert alert-success"><?= $this->session->flashdata('sendorder') ?></div>
                <?php endif ?>


                                                
                <div class="checkout_details_area mt-50 clearfix">

                    <div class="cart-page-heading mb-30">
                        <!-- <h5>Detail pembayaran</h5> -->
                    </div>
                    <?php if ($this->session->userdata('username') !== NULL): ?>
                    <div class="card mb-4">
                        <div class="card-body">
                            <h6>Selamat Datang</h6>
                            <h4><?= $this->session->userdata('username') ?></h4>
                            <div class="float-right">
                                <a href="<?= base_url('login/logout?checkout=1') ?>" class="btn essence-btn">Logout</a>
                            </div>
                        </div>
                    </div>
                        <?php else: ?>
                            <?php if (@$_GET['login']): ?>
                                <h6>Login</h6>
                            <form method="POST" action="<?= base_url('login/login?from=cart') ?>">
                            
                            <div class="input-group mb-3">
                                <input type="text" name="username" class="form-control" placeholder="Username">
                            </div>
                            <div class="input-group mb-3">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <?php if($this->session->flashdata('message')){?>
                                <div class="alert alert-warning"><?= $this->session->flashdata('message') ?></div>
                            <?php } ?>
                            <p><a href="<?= base_url('lupa-password') ?>" class="text-dark">Lupa Password?</a></p>

                            <div class="row">
                                <!-- /.col -->
                                <div class="col-4">
                                    <button type="submit" class="btn essence-btn">MASUK</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form>
                                <?php else: ?>

                            <div class="card mb-4">
                                <div class="card-body">
                                    <h6>Apakah anda sudah punya akun? silahkan</h6>
                                    <a class="btn essence-btn" href="<?= base_url('checkout?login=1') ?>">Login</a>
                                </div>
                            </div>

                         <h6>Atau silahkan register disini</h6>
                            <form method="POST" action="<?= base_url('login/proses_daftar?from=cart') ?>">
                            <input type="hidden" value="avatar.png" name="user_image">
                            <div class="input-group mb-3">
                                <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" name="alamat" class="form-control" placeholder="Alamat" required>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" name="ktp" class="form-control" placeholder="Nomor KTP" required>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" name="no_telp" class="form-control" placeholder="No Telepon" required>
                            </div>
                            <div class="input-group mb-3">
                                <input type="email" name="email" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" name="username" class="form-control" placeholder="Username" required>
                            </div>
                            <div class="input-group mb-3">
                                <input type="password" name="password" class="form-control" placeholder="Password" required>
                            </div>

                            <div class="row">
                                <!-- /.col -->
                                <div class="col-4">
                                    <button type="submit" class="btn essence-btn">REGISTER</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form>
                            <?php endif ?>
                    <?php endif ?>
                                                           
                    
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-5">
                <div class="order-details-confirmation">

                    <div class="cart-page-heading">
                        <h5>Detail pesanan</h5>
                    </div>

                    <ul class="order-details-form mb-4">
                        <li><span>Produk</span> <span>Total</span></li>
                        <?php 
                        $grandTotal = NULL;

                        if($this->session->userdata('cart') != NULL){ foreach ($this->session->userdata('cart') as $key=>$item): ?>
                        <li>
                            <span>
                                    <?= $item['title'] ?>
                                    <br>
                                   <?= $fx->_rupiah($item['harga']) ?> &#x2715; <?= $item['total'] ?>
                                </span>
                            <span>
                                    <?= $fx->_rupiah($item['harga']*$item['total']) ?>
                                    <?php $grandTotal+= ($item['harga']*$item['total']) ?>
                                </span>
                        </li>
                        <?php endforeach; }else{ echo "Tidak Ada Barang."; } ?>
                                                 <li><span>Total</span> <span><?= $fx->_rupiah($grandTotal) ?></span></li>
                    </ul>
                    <?php if ($this->session->userdata('username') !== NULL): ?>
                        <?php if ($this->session->userdata('cart') != NULL): ?>
                            <form class="needs-validation" action="<?= base_url('konfirmasi-pembayaran') ?>" novalidate>

                            <input type="submit" class="btn essence-btn" value="Bayar">
                            </form>
                            <?php else: ?>
                                Silahkan Pilih Barang untuk Melakukan Pembayaran
                        <?php endif ?>
                    <?php else: ?>
                        Silahkan Login Terlebih dahulu
                         <?php endif ?>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- ##### Checkout Area End #####