<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb_area bg-img" style="background-image: url(http://localhost/project/ta_vinokha/public/front/img/bg-img/breadcumb.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="page-title text-center">
                    <h2>Lupa Password</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Checkout Area Start ##### -->
<div class="checkout_area section-padding-80">
    <div class="container">
                <div class="row">

            <div class="col-12 col-md-6">
                 
                <div class="cart-page-heading mb-30">
                    <h5>Lupa Password</h5>
                </div>
                <p>Masukan Email Pengguna, Password akan dikirim ke email terdaftar!</p>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control form-email" placeholder="Masukan Email anda..." required>
                </div>
                <div class="form-group form-d-none-warning" style="display: none">
                    <p class="alert alert-warning form-alert"></p>
                </div>
                <div class="form-group form-d-none-success" style="display: none">
                    <p class="alert alert-success form-alert"></p>
                </div>
                <button class="btn btn-primary btn-lupapass"><i style="display: none" class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Lupa Password</button>
            </div>
        </div>
    </div>
</div>


<!-- ##### Checkout Area End #####