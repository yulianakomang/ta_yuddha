<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb_area bg-img" style="background-image: url(http://localhost/project/ta_vinokha/public/front/img/bg-img/breadcumb.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="page-title text-center">
                    <h2>Konfirmasi Pembayaran</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Checkout Area Start ##### -->
<div class="checkout_area section-padding-80">
    <div class="container">
                <div class="row">

            <div class="col-12 col-md-6">
                 
                <div class="cart-page-heading mb-30">
                    <h5>Konfirmasi Pembayaran</h5>
                </div>
                <p>Silahkan melakukan Konfirmasi pembayaran! Orderan akan kami terima ketika sudah melakukan Konfrimasi Pembayaran.</p>
                <div class="order-details-confirmation">

                    <div class="cart-page-heading">
                        <h5>Detail pesanan</h5>
                    </div>
                    <?php if ($this->session->userdata('username') == NULL) {
                        header('location: '.base_url('/checkout'));
                    } ?>

                    <ul class="order-details-form mb-4">
                        <li><span>Produk</span> <span>Total</span></li>
                        <?php 
                        $grandTotal = NULL;

                        if($this->session->userdata('cart') != NULL){ foreach ($this->session->userdata('cart') as $key=>$item): ?>
                        <li>
                            <span>
                                    <?= $item['title'] ?>
                                    <br>
                                   <?= $fx->_rupiah($item['harga']) ?> &#x2715; <?= $item['total'] ?>
                                </span>
                            <span>
                                    <?= $fx->_rupiah($item['harga']*$item['total']) ?>
                                    <?php $grandTotal+= ($item['harga']*$item['total']) ?>
                                </span>
                        </li>
                        <?php endforeach; }else{ echo "Tidak Ada Barang."; } ?>
                                                 <li><span>Total</span> <span><?= $fx->_rupiah($grandTotal) ?></span></li>
                    </ul>
                    
                    <form action="<?= base_url('Front/proses_konfirmasi_pembayaran') ?>" method="post" id="form_konfir_pembayaran" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Upload Bukti Transfer</label>
                        <input type="file" name="upload_bukti" id="upload_bukti" class="form-control">
                    </div>
                    <div class="form-group form-d-none-warning" style="display: none">
                        <p class="alert alert-warning form-alert"></p>
                    </div>
                    <div class="form-group">
                        <div class="float-right">
                            <button class="btn btn-success btn_konfir_pembayaran">Konfirmasi Pembayaran</button>
                        </div>
                    </div>
                    </form>
                    <div style="padding: 10px;clear: both;"></div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>


<!-- ##### Checkout Area End #####