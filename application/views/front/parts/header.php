<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>ACHALESVARA - Official Website</title>

    <!-- Favicon  -->
    <link rel="icon" href="<?= base_url('asset/front') ?>/img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="<?= base_url('asset/front') ?>/css/core-style.css">
    <link rel="stylesheet" href="<?= base_url('asset/front') ?>/style.css">
    <style>
        .single-product-wrapper .product-img{
            height: 260px;
            overflow: auto !important;
        }
    </style>

</head>

<body>
    <!-- ##### Header Area Start ##### -->
    <header class="header_area">
        <div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between">
            <!-- Classy Menu -->
            <nav class="classy-navbar" id="essenceNav">
                <!-- Logo -->
                <a class="nav-brand" href="<?= base_url()?>">ACHALESVARA</a>
                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>
                <!-- Menu -->
                <div class="classy-menu">
                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>
                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul>
                            <li><a href="<?= base_url()?>">HOME</a></li>
                            <li><a href="<?= base_url('kategori')?>">Produk</a></li>
                            <li><a href="<?= base_url('tentang')?>">Tentang Saya</a></li>
                            <li><a href="<?= base_url('kontak')?>">Kontak</a></li>
                        </ul>
                    </div>
                    <!-- Nav End -->
                </div>
            </nav>

            <!-- Header Meta Data -->
            <div class="header-meta d-flex clearfix justify-content-end">
                <!-- User Login Info -->
                <div class="user-login-info">
                    <a href="<?= base_url('checkout') ?>"><img src="<?= base_url('asset/front') ?>/img/core-img/user.svg" alt=""></a>
                </div>
                <!-- Cart Area -->
                <div class="cart-area">
                    <a href="#" id="essenceCartBtn"><img src="<?= base_url('asset/front') ?>/img/core-img/bag.svg" alt=""> <span><?= @count($this->session->userdata('cart')) ?></span></a>
                </div>
            </div>

        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Right Side Cart Area ##### -->
    <div class="cart-bg-overlay"></div>

    <div class="right-side-cart-area">

        <!-- Cart Button -->
        <div class="cart-button">
            <a href="#" id="rightSideCart"><img src="<?= base_url('asset/front') ?>/img/core-img/bag.svg" alt=""> <span><?= @count($this->session->userdata('cart')) ?></span></a>
        </div>

        <div class="cart-content d-flex">

            

            <!-- Cart Summary -->
            <div class="cart-amount-summary">

                <h2>Keranjang</h2>
                <div class="cart-lists">

                    <?php if($this->session->userdata('cart') != NULL){ foreach ($this->session->userdata('cart') as $key=>$item): ?>
                    <div class="cart-list-item pb-2">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="<?= $item['img'] ?>" width="200px">
                            </div>
                            <div class="col-md-8">
                                <b><?= $item['title'] ?></b> <br>
                                <span><?= $fx->_rupiah($item['harga']) ?></span> <br>
                                <span><?= $item['total'] ?>x</span>
                                <div class="clear_cart" data-id="<?= $key ?>" style="position: absolute;top: 0;right: 0;cursor: pointer;">&times;</div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; }else{ echo "Tidak Ada Barang."; } ?>


                </div>
                <div class="checkout-btn mt-100">
                    <a href="<?= base_url('checkout') ?>" class="btn essence-btn">Bayar Sekarang</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Right Side Cart End ##### -->
