<!-- Single Blog Post Thumb -->
    <div class="single-blog-post-thumb">
        <img src="https://images.unsplash.com/photo-1441986300917-64674bd600d8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80" alt="" style="object-fit: cover;
            object-position: center center;
            width:100%;
            height: 300px;">
    </div>
<div class="contact-area d-flex align-items-center">

        <div class="google-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.9902991884437!2d116.17938091488887!3d-8.596928693821331!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dcdb999f2735c51%3A0x75d831fb4e34c6cc!2sAchalesvara!5e0!3m2!1sen!2sid!4v1594298764169!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>

        <div class="contact-info">
            <h2>Temukan Kami</h2>
            <p>Karang Taliwang, Gg. Klp. Gading No.7, Dasan Tereng, Narmada, Kabupaten Lombok Barat, Nusa Tenggara Bar. 83371</p>

            <div class="contact-address mt-50">
                <p><span>telephone:</span> 081239390200</p>
                <p><span>Email:</span> achalesvara20@gmail.com</p>
                <p></p>
            </div>
        </div>

    </div>