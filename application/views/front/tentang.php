<div class="single-blog-wrapper">

    <!-- Single Blog Post Thumb -->
    <div class="single-blog-post-thumb">
        <img src="https://images.unsplash.com/photo-1441986300917-64674bd600d8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80" alt="" style="object-fit: cover;
            object-position: center center;
            width:100%;
            height: 300px;">
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="regular-page-content-wrapper section-padding-80">
                    <div class="regular-page-text">
                       <h2>Tentang Kami</h2>
                        <p align="justify">Achalesvara merupakan UMKM yang berjalan di bidang peternakan ayam dan penjualan berbagai obat-obatan seperti vitamin, vaksin, dan lainnya serta menjual makanan ternak ayam dan setiap bulannya terjual 20 sak pakan ayam. Peternakan ini di dirikan langsung oleh Agung Devarya Krishna. Selain menjual berbagai perlengkapan ternak ayam, Achalesvara juga memiliki peternakan ayam petelur yang setiap harinya dapat menghasilkan telur ayam ras hingga enam ikat telur yang 1 ikatnya berisi 30 butir telur. Peternakan achalesvara berlokasi di Gg Kelapa Gading No 7, Karang Taliwang, Dasan Tereng, Provinsi NTB. </p>

                        <blockquote>
                            <h6><i class="fa fa-quote-left" aria-hidden="true"></i>ACHALESVARA</span>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>