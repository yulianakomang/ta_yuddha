<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('data_model', 'data');
		$this->load->model('Pelanggan');
	}

	public function index(){
		if ($this->session->userdata('username') != null) {
			redirect('/admin');
		}else 
			$this->load->view('auth/login');
	}

	public function login(){
		$login = $this->input->post();
		$admin = $this->data->get_admin($login['username'],$login['password']);
		$user = $this->Pelanggan->get_user($login['username'],$login['password']);

		print_r(count($user->result()));
		// die();

		if (count($admin->result()) > 0 ) {
			$data = $admin->row();
			$this->session->set_userdata(array(
				'id' => $data->id,
				'username' => $data->username,
				'nama' => $data->nama,
				'level' => $data->level,
			));
			redirect('admin');
		}else if (count($user->result()) > 0 ) {
			$data = $user->row();

			// remove data reset jika berhasil login
			$data_reset = @$_SESSION['user_reset'][0];
			if ($data_reset->email == $data->email) {
				unset($_SESSION['user_reset']);
			}


			$this->session->set_userdata(array(
				'id' => $data->id,
				'username' => $data->username,
				'nama' => $data->nama,
				'data' => $data,
				'level' => '2',
			));
			redirect('checkout');

		}else{
			$this->session->set_flashdata([
				'error' => true,
				'message' => 'Username tidak ditemukan atau password salah!'
			]);
			if (@$_GET['admin']) {
				redirect('/login');
			}
			redirect('/checkout?login=1');
		}
		// print_r($this->session);
	}

	public function daftar()
	{
		$this->load->view('auth/daftar');
	}
	public function proses_daftar()
	{
		$input = (object) $this->input->post();
		$this->load->model('Pelanggan');
		$registered = count($this->Pelanggan->get_where(array('email'=>$input->email)));
		if ($registered > 0) {
			$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah','info'=>'Akun anda sudah terdaftar! silahkan Login!'));
			return redirect(base_url('checkout'),'refresh');
		}else{
			$id = $this->Pelanggan->insert($input);

			// Store to Session
			$this->session->set_userdata(array(
				'id' => $id,
				'username' => $input->username,
				'nama' => $input->nama,
				'level' => '2',
			));


			$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah','info'=>'Akun Berhasil dibuat! silahkan Belanja!'));
			return redirect(base_url('checkout'),'refresh');
		}
	}

	public function logout(){
		// $this->session->sess_destroy();
		$this->session->unset_userdata(array(
				'id',
				'username',
				'nama',
				'level',
			));
		if (@$_GET['checkout']) {
			redirect('checkout');
		}else{
			redirect('/');
		}
	}

}