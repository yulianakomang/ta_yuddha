<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Penjualan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?= base_url('asset/') ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?= base_url('asset/') ?>sweetalert2/dist/sweetalert2.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body>

<!-- Content Wrapper. Contains page content -->
<div class="">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <br>
          <h1 style="text-align: center;">DETAIL PENJUALAN </h1>
          <div class="card-header">
          </div>
          <!-- /.card-header -->
          <div class="card-body">
          
            <div class="row">
              <div class="col-md-6" style="font-size: 20px;font-weight: 600;margin-bottom: 10px">No Faktur <br> <p style="padding: 3px;background-color: #2d2d2d;color: white;"><?= $this->uri->segment(3); ?></p></div>
              <div class="col-md-6" style="font-size: 20px;font-weight: 600;margin-bottom: 10px">
                Pelanggan : <?php foreach ($pelanggan_list as $plg): ?>
                      <?php if ($plg->id == $penjualan[0]->id_pelanggan): ?>
                        <?= $plg->nama ?>
                      <?php endif ?>
                    <?php endforeach ?><br>
                Tanggal Order: <?= date('d-m-Y',strtotime($penjualan[0]->tanggal_order)) ?> <br>
                Tanggal Terima: <?= date('d-m-Y',strtotime($penjualan[0]->tanggal_terima)) ?>
                  
                </div>
            </div>
            <table class="table table-bordered table-striped ">
              <thead>
                <tr>               
                  <th>Kode Produk </th>
                  <th>Kategori</th>
                  <th>Harga Jual </th>
                  <th>Jumlah Pesanan </th>
                  <th>Sub Total</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($list as $item): ?>

                  <tr>
                  <td><?php foreach ($produk_list as $key => $pro): ?>
                    <?php if ($pro->id == $item->id_produk): ?>
                      <?= '[ '.$pro->kode_produk.' ] '.$pro->nama ?>
                    <?php endif ?>
                  <?php endforeach ?></td>
                  
                  <td>
                    <?php foreach ($produk_list as $key => $pro): ?>
                      <?php if ($pro->id == $item->id_produk): ?>
                        <?php foreach ($kategori_list as $key => $cat): ?>
                          <?php if ($pro->kategori_id == $cat->id): ?>
                            <?= $cat->nama_kategori ?>
                          <?php endif ?>
                        <?php endforeach ?>
                      <?php endif ?>
                    <?php endforeach ?>
                  </td>             
                    
                    <td><?php foreach ($produk_list as $key => $pro): ?>
                      <?php if ($pro->id == $item->id_produk): ?>
                        Rp.<?= number_format($pro->harga_jual,0,'','.') ?>
                      <?php endif ?>
                    <?php endforeach ?></td>
                    <td><?= $item->jumlah ?></td>
                    <td>Rp. <?= number_format($item->subtotal,0,'','.') ?></td>
                    
                    
                  </tr>
                <?php endforeach ?>
                
              </tfoot>
            </table>
            <br>
            <div class="row">
              <div class="col-md-4 offset-8">
                <table class="table">
                  <tr>
                    <td>Total Harga</td>
                    <td>:</td>
                    <td>Rp. <?= number_format(@$penjualan[0]->total,0,'','.') ?></td>
                  </tr>
                  <tr>
                    <td>Uang Muka</td>
                    <td>:</td>
                    <td>Rp. <?= number_format(@$penjualan[0]->uang_muka,0,'','.') ?></td>
                  </tr>
                  <tr>
                    <td>Sisa Pembayaran</td>
                    <td>:</td>
                    <td>Rp. <?= number_format(@$penjualan[0]->total-$penjualan[0]->uang_muka,0,'','.') ?></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card"><br>
          <h1 style="text-align: center;">DETAIL PEMBAYARAN</h1>
          <div class="card-header">
          </div>
            <table class="table">
              <tr>
                <th>Kode Pembayaran</th>
                <td>:</td>
                <td><?= (@$pembayaran[0]->kode === NULL)?'Belum Bayar':$pembayaran[0]->kode; ?></td>
              </tr>
              <tr>
                <th>Tanggal Pembayaran</th>
                <td>:</td>
                <td><?= (@$pembayaran[0]->tanggal_pembayaran === NULL)?'Belum Bayar':date('d-m-Y',strtotime($pembayaran[0]->tanggal_pembayaran)); ?></td>
              </tr>
              <tr>
               <th>pelunasan Pembayaran</th>
                <td>:</td>
                <td><?= (@$pembayaran[0]->sisa_pembayaran === NULL)?'Belum Bayar': 'Rp. '. number_format($pembayaran[0]->sisa_pembayaran,0,'','.'); ?></td>
              </tr>
              <tr>
               <th>Keterangan</th>
                <td>:</td>
                <td><?php if (@$penjualan[0]->status != 'Lunas'): ?>Belum Lunas <?php else: ?> Lunas <?php endif ?></td>
              </tr>
            </table>
            
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
  window.print();
  window.onfocus=function(){ window.close();}
</script>

</body>
</html>