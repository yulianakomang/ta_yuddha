<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Model {

	protected $table = 'tb_produk';
    protected $produk_id='';

    public function min_stok($id='',$nilai='')
    {
        // ambil stok saat ini
        $data = $this->get_where(array('id'=>$id));
        $hasil = $data[0]->stok - $nilai;
        //update stok
        $this->db->where('id',$id);
        $this->db->update($this->table,array('stok'=>$hasil));
    }
    public function add_stok($id='',$nilai='')
    {
        // ambil stok saat ini
        $data = $this->get_where(array('id'=>$id));
        $hasil = $data[0]->stok + $nilai;
        //update stok
        $this->db->where('id',$id);
        $this->db->update($this->table,array('stok'=>$hasil));
    }

    public function removeDot($value='')
    {
        return str_replace('.', '', $value);
    }
    public function getRandom()
    {
        $this->db->order_by('id', 'RANDOM');
        return $this->db->get($this->table)->result();
    }
    public function getNews()
    {
        $this->db->order_by('id', 'DESC');
        return $this->db->get($this->table)->result();
    }

	public function fetchAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->result();
    }
    public function countAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->num_rows();
    }

    public function get_product_all($where = null){
        $this->db->select('tb_produk.*, tb_kategori.nama_kategori');
        $this->db->from('tb_produk');
        $this->db->join('tb_kategori', 'tb_produk.kategori_id = tb_kategori.id');
        if ($where != null && is_array($where)) { 
            $this->db->where($where);
        }
        return $this->db->get()->result();
    }

    public function get_where($data = '')
    {
        $this->db->where($data);
        return $this->db->get($this->table)->result();
    }


    public function getTotalTerjual($id,$start='',$end='')
    {
        $this->db->select_sum('jumlah');
        if ($start != '' && $end != '') {
            $this->db->where('tgl_order >=',$start);
            $this->db->where('tgl_order <=',$end);
        }
        $this->db->where(array('id_produk'=>$id));
        return $this->db->get('tb_detail_penjualan')->row()->jumlah;
    }


    public function insert($input = '')
    {
        $this->produk_id = $input->kode_produk;
        $data = array(
            'kode_produk'         => $input->kode_produk,
            'nama'         => $input->nama,
            'keterangan'         => $input->keterangan,
            'harga'         => $this->removeDot($input->harga),
            'gambar'         => $this->_uploadImage(),
            'satuan'         => $input->satuan,
            'stok'         => $input->stok,
            'harga_jual'         => $this->removeDot($input->harga_jual),
            'kategori_id'         => $input->kategori_id,
            
            );
        $this->db->insert($this->table, $data);
    }

    public function update($input = '',$where)
    {
        $this->produk_id = $input->kode_produk;   
        $data = array(
            'kode_produk'         => $input->kode_produk,
            'nama'         => $input->nama,
            'keterangan'         => $input->keterangan,
            'harga'         => $this->removeDot($input->harga),
            'satuan'         => $input->satuan,
            'stok'         => $input->stok,
            'harga_jual'         => $this->removeDot($input->harga_jual),
            'kategori_id'         => $input->kategori_id,
        );
        $size= $_FILES['gambar']['size'];

        if ($size > 0) {
            $this->db->select('gambar');
            $this->db->where(array('kode_produk'=>$input->kode_produk));
            $old_img = $this->db->get($this->table)->result();

            @unlink('./uploads/'.$old_img[0]->gambar);

            $data['gambar'] = $this->_uploadImage();
        }
       
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function delete($where)
    {   
        $this->db->select('gambar');
        $this->db->where($where);
        $old_img = $this->db->get($this->table)->result();

        @unlink('./uploads/'.$old_img[0]->gambar);

        $this->db->delete($this->table,$where);
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png';
        $config['file_name']            = $this->produk_id;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('gambar')) {
            return $this->upload->data("file_name");
        }
        
        return "default.jpg";
    }

}