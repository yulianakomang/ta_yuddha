    <!-- ##### Footer Area Start ##### -->
    <footer class="footer_area clearfix">
        <div class="container">
            <div class="row">
                <!-- Single Widget Area -->
                <div class="col-12 col-md-6">
                    <div class="single_widget_area d-flex mb-30">
                        <!-- Logo -->
                        <div class="footer-logo mr-50">
                            <a style="font-size: 20px" href="<?= base_url()?>">ACHALESVARA</a>
                        </div>
                        <!-- Footer Menu -->
                        <div class="footer_menu d-none d-sm-block">
                            <ul>
                                <li><a href="<?= base_url()?>">HOME</a></li>
                                <li><a href="<?= base_url('kategori')?>">Produk</a></li>
                                <li><a href="<?= base_url('tentang')?>">Tentang Saya</a></li>
                                <li><a href="<?= base_url('kontak')?>">Kontak</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Single Widget Area -->
               <div class="col-12 col-md-6">
                    <div class="single_widget_area">
                        <div class="footer_social_area">
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row align-items-end">
                <!-- Single Widget Area -->
                <!-- Single Widget Area -->
                
                
            </div>

<div class="row mt-5">
                <div class="col-md-12 text-center">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>

        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="<?= base_url('asset/front') ?>/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?= base_url('asset/front') ?>/js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?= base_url('asset/front') ?>/js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="<?= base_url('asset/front') ?>/js/plugins.js"></script>
    <!-- Classy Nav js -->
    <script src="<?= base_url('asset/front') ?>/js/classy-nav.min.js"></script>
    <!-- Active js -->
    <script src="<?= base_url('asset/front') ?>/js/active.js"></script>

    <script type="text/javascript">
        $('.add-to-cart').click(function() {
            var data = {
                id: $(this).data('id'),
                code: $(this).data('code'),
                title: $(this).data('title'),
                img: $(this).data('img'),
                harga: $(this).data('harga'),
            }
            var url ="<?= base_url('Front/add_to_cart') ?>";
            $.post(url,data,function(ev){
                console.log(ev);
                location.reload();
            });
        });
        $('.min-to-cart').click(function() {
            var data = {
                id: $(this).data('id'),
                code: $(this).data('code'),
                title: $(this).data('title'),
                img: $(this).data('img'),
                harga: $(this).data('harga'),
            }
            var url ="<?= base_url('Front/min_to_cart') ?>";
            $.post(url,data,function(ev){
                console.log(ev);
                location.reload();
            });
        });
        $('.clear_cart').click(function(){
            var url ="<?= base_url('Front/clear_cart') ?>";
            var id = $(this).data('id');
            $.get(url+'?id='+id,function(ev){
                console.log(ev);
                location.reload();
            });
        });
        /* Lupa Pass */
        $('.btn-lupapass').click(function(){
            $('.form-d-none-warning').css('display','none');
            $('.form-d-none-success').css('display','none');
            $('.fa-spin').css('display','none');
            var url ="<?= base_url('Front/lupapass_aksi') ?>";
            var email = $('.form-email').val();
            var dataString = {
                email:email
            }
            if (email.indexOf('@') == -1) {
                $('.form-d-none-warning').css('display','block');
                $('.form-alert').html('Mohon Check format Email..');
            }else{
                $('.fa-spin').css('display','inline-block');
                $.post(url,dataString,function(ev){
                    console.log(ev);
                    if (ev == 'gagal') {
                        $('.form-d-none-warning').css('display','block');
                        $('.form-alert').html('Email tidak terdaftar..');
                    }else{
                        $('.form-d-none-success').css('display','block');
                        $('.form-alert').html('Permohonan Password sudah dikirim ke Email anda..');
                    }
                    $('.fa-spin').css('display','none');
                });
            }
        });

        /*Reset Pass*/
        $('.btn-resetpass').click(function(){
            $('.form-d-none-warning').css('display','none');
            $('.form-d-none-success').css('display','none');
            var url ="<?= base_url('Front/reset_password_aksi') ?>";
            var pass = $('.form-password').val();
            var pass_valid = $('.form-password-valid').val();
            if (pass == '' || pass_valid == '') {
                $('.form-d-none-warning').css('display','block');
                $('.form-alert').html('Mohon jangan dikosongkan..');
            }else{
                if (pass !== pass_valid) {
                    $('.form-d-none-warning').css('display','block');
                    $('.form-alert').html('Password baru dan Konfirmasinya harus sama..');  
                }else{
                    if (pass.length < 8) {
                        $('.form-d-none-warning').css('display','block');
                        $('.form-alert').html('Password harus lebih dari 8 digit..'); 
                    }else{
                        var dataString = {
                            pass : pass
                        }
                        $.post(url,dataString,function(ev){
                            console.log(ev);
                            if (ev == 'gagal') {
                                $('.form-d-none-warning').css('display','block');
                                $('.form-alert').html('Terjadi Kesalahan..');
                            }else{
                                $('.form-d-none-success').css('display','block');
                                $('.form-alert').html('Password Berhasil diperbaharui silahkan login..');
                                window.setTimeout(function(){ window.location = "<?= base_url('/checkout') ?>"; },1000);
                            }
                        }); 
                    }  
                }
            }
        });


        $('#form_konfir_pembayaran').submit(function(ev) {
            $('.form-d-none-warning').css('display','none');
            var file = $('#upload_bukti').val();
            if (file !== '') {
                return true;
            }else{
                ev.preventDefault();
                $('.form-d-none-warning').css('display','block');
                $('.form-alert').html('Masukan terlebih dahulu bukti transfer..');
            }

        });
    </script>

</body>

</html>