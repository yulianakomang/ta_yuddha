<div style="padding: 20px;clear: both;"></div>
<!-- ##### Single Product Details Area Start ##### -->
    <section class="single_product_details_area d-flex align-items-center container">

        <!-- Single Product Thumb -->
        <div class="single_product_thumb clearfix">
            <div class="product_thumbnail_slides owl-carousel">
                <img src="<?= base_url('uploads/'.$hasil->gambar) ?>" alt="">
                <img src="<?= base_url('uploads/'.$hasil->gambar) ?>" alt="">
                <img src="<?= base_url('uploads/'.$hasil->gambar) ?>" alt="">
            </div>
        </div>

        <!-- Single Product Description -->
        <div class="single_product_desc clearfix">
            <span><?= $fx->_kategori($hasil->id)->nama_kategori ?></span>
            <h2><?= $hasil->nama ?></h2>
            <p class="product-price"><?= $fx->_rupiah($hasil->harga_jual) ?></p>
            <p class="product-desc" style="text-align: justify;"><?= $hasil->keterangan ?></p>
            <p>Stok Saat ini: <?= $hasil->stok??0 ?></p>
            <?php 
            $cartItems = $this->session->userdata('cart');
            $exists = @count($cartItems['id-'.$hasil->id]);
            $item = @$cartItems['id-'.$hasil->id];
            $future_stok= ($item['total']+1);
            if ($exists != 0): ?>
                <button class="btn btn-default min-to-cart" data-id="<?= $item['id'] ?>" data-code="<?= $item['code'] ?>" data-title="<?= $item['title'] ?>" data-img="<?= $item['img'] ?>" data-harga="<?= $item['harga'] ?>">-</button>
                <input type="text" class="form-control" readonly style="width: 50px;display: inline;" value="<?= $item['total'] ?>">
                <?php  if ($future_stok <= $hasil->stok): ?>
                <button class="btn btn-default add-to-cart" data-id="<?= $item['id'] ?>" data-code="<?= $item['code'] ?>" data-title="<?= $item['title'] ?>" data-img="<?= $item['img'] ?>" data-harga="<?= $item['harga'] ?>">+</button>
                <?php else: ?>
                    Stok Habis
                <?php endif ?>
                <div class="mb-3"></div>
                <?php else: ?>
                    <?php  if ($future_stok <= $hasil->stok): ?>

                <?php endif ?>
            <?php endif ?>

            <!-- Form -->
            <form class="cart-form clearfix" method="post">
                <!-- Select Box -->
                <!-- <div class="select-box d-flex mt-50 mb-30">
                    <select name="select" id="productSize" class="mr-5">
                        <option value="value">Size: XL</option>
                        <option value="value">Size: X</option>
                        <option value="value">Size: M</option>
                        <option value="value">Size: S</option>
                    </select>
                    <select name="select" id="productColor">
                        <option value="value">Color: Black</option>
                        <option value="value">Color: White</option>
                        <option value="value">Color: Red</option>
                        <option value="value">Color: Purple</option>
                    </select>
                </div> -->
                <!-- Cart & Favourite Box -->
                <div class="cart-fav-box d-flex align-items-center">
                    <!-- Cart -->
                    <?php if ($future_stok <= $hasil->stok): ?>
                        
                    <a href="#" class="btn essence-btn add-to-cart" data-id="<?= $hasil->id ?>" data-code="<?= $hasil->kode_produk ?>" data-title="<?= $hasil->nama ?>" data-img="<?= base_url('uploads/'.$hasil->gambar) ?>" data-harga="<?= $hasil->harga_jual ?>">Beli</a>
                    <?php endif ?>
                    <?php if ($exists != 0): ?>
                        <a class="btn essence-btn btn-success" href="<?= base_url('checkout') ?>">Bayar</a>
                    <?php endif ?>
                    <!-- Favourite -->
                    <!-- <div class="product-favourite ml-4">
                        <a href="#" class="favme fa fa-heart"></a>
                    </div> -->
                </div>
            </form>
        </div>
    </section>
    <div style="padding: 20px;clear: both;"></div>
    <!-- ##### Single Product Details Area End ##### -->