<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MyController.php';

class Front extends MyController {

	public function __construct(){
		parent::__construct();
		$this->load->model('Produk');
		$this->load->model('Kategori');
		$this->load->model('Pembayaran');
		$this->load->model('Penjualan');
		$this->load->model('Penjualan_detail');
		$this->load->model('Pelanggan');
		$this->load->helper('url');
	}

	/* Halaman Home */
	public function home()
	{
		$data['popular'] = $this->Produk->getRandom();
		$data['terbaru'] = $this->Produk->getNews();
		$data['fx'] = $this;

		$this->load->view('front/parts/header',$data);
		$this->load->view('front/home');
		$this->load->view('front/parts/footer');
	}

	/* Halaman Kategori */
	public function kategori()
	{

		$data['terbaru'] = $this->Produk->getNews();
		$data['fx'] = $this;


		$this->load->view('front/parts/header',$data);
		$this->load->view('front/kategori');
		$this->load->view('front/parts/footer');
	}

	/* Halaman Detail Produk */
	public function detail( $kode ='')
	{
		if ($kode == '') {
			return redirect(base_url());
		}
		$data['hasil'] = $this->Produk->get_where(array('kode_produk'=>$kode))[0];
		$data['fx'] = $this;

		$this->load->view('front/parts/header',$data);
		$this->load->view('front/detail');
		$this->load->view('front/parts/footer');
	}

	/* Halaman Tentang */
	public function tentang()
	{
		$data['fx'] = $this;
		$this->load->view('front/parts/header',$data);
		$this->load->view('front/tentang');
		$this->load->view('front/parts/footer');
	}

	/* Halaman Kategori */
	public function kontak()
	{
		$data['fx'] = $this;
		$this->load->view('front/parts/header',$data);
		$this->load->view('front/kontak');
		$this->load->view('front/parts/footer');
	}

	/*======== Helper/Fungsi Pembantu ========= */

	public function _rupiah($nominal='')
	{
		return "Rp " . number_format($nominal,0,',','.');
	}

	public function _kategori($id='')
	{
		$data = $this->Produk->get_where(array('id'=>$id));
		$cat = $this->Kategori->get_where(array('id'=>$data[0]->kategori_id));
		return $cat[0];
	}


	/* Add to Cart */
	public function add_to_cart()
	{
		// New
		$input = (object) $this->input->post(NULL);
		$data = array();
		// Jika ada barang di keranjang
		if ($this->session->userdata('cart') != NULL && count($this->session->userdata('cart')) > 0 ) {

			$cartItems = $this->session->userdata('cart');
			$exists = @count($cartItems['id-'.$input->id]);

			if ($exists == 0) { // Jika item tidak ada
				$add['id-'.$input->id] = array(
					'id'=> $input->id,
					'code'=> $input->code,
					'title'=> $input->title,
					'img'=> $input->img,
					'harga'=> $input->harga,
					'total'=> 1,
				);
				$data = array_merge($cartItems, $add); // tambah item
			}else{ // jika item ada
				$total = $cartItems['id-'.$input->id]['total']+1; // tambah jumlah item
				$update['id-'.$input->id] = array(
					'id'=> $input->id,
					'code'=> $input->code,
					'title'=> $input->title,
					'img'=> $input->img,
					'harga'=> $input->harga,
					'total'=> $total,
				);
				$data = array_merge($cartItems, $update);
			}
			$this->session->set_userdata('cart',$data);

		}else{
			$data['id-'.$input->id] = array(
				'id'=> $input->id,
				'code'=> $input->code,
				'title'=> $input->title,
				'img'=> $input->img,
				'harga'=> $input->harga,
				'total'=> 1,
			);
			$this->session->set_userdata('cart',$data);

		}


		return 'success';
	}

	/* Min to Cart/Keranjang */
	public function min_to_cart()
	{
		// New
		$input = (object) $this->input->post(NULL);
		$data = array();
		// Jika ada barang di keranjang
		if ($this->session->userdata('cart') != NULL && count($this->session->userdata('cart')) > 0 ) {

			$cartItems = $this->session->userdata('cart');
			$exists = @count($cartItems['id-'.$input->id]);

			if ($exists == 0) { // Jika Item Kosong
				unset($_SESSION['cart']['id-'.$input->id]); // Hapus Item
				return 'success';
				die();
			}else{ // Jika Item ada
				$total = $cartItems['id-'.$input->id]['total']-1; // Kurangi jumlah item
				if ($total == 0) {
					unset($_SESSION['cart']['id-'.$input->id]);
					return 'success';
					die();
				}
				$update['id-'.$input->id] = array(
					'id'=> $input->id,
					'code'=> $input->code,
					'title'=> $input->title,
					'img'=> $input->img,
					'harga'=> $input->harga,
					'total'=> $total,
				);
				$data = array_merge($cartItems, $update);
			}
			$this->session->set_userdata('cart',$data);

		}


		return 'success';
	}


	public function clear_cart($value='')
	{
		$id = $this->input->get('id');
		unset($_SESSION['cart'][$id]);
	}

	/* Halaman Checkout */
	public function checkout()
	{
		$data['fx'] = $this;
		$data['db_produk'] = $this->Produk;
		$this->load->view('front/parts/header',$data);
		$this->load->view('front/checkout');
		$this->load->view('front/parts/footer');
	}

	/* Halaman Konfirmasi Pembayaran */
	public function konfirmasi_pembayaran()
	{
		$data['fx'] = $this;
		$this->load->view('front/parts/header',$data);
		$this->load->view('front/pembayaran');
		$this->load->view('front/parts/footer');
	}
	/* Proses Konfirmasi Pembayaran */
	public function proses_konfirmasi_pembayaran()
	{
		$input = (object) $this->input->post(NULL);
		$cartItems = $this->session->userdata('cart');
		$grandTotal = NULL;
		$nomor_faktur = 'PJ-'.date('Ymdhis');
		

		if($this->session->userdata('cart') != NULL){ 
			foreach ($cartItems as $key=>$item){
				$grandTotal+= ($item['harga']*$item['total']);
				$currentProductData = $this->Produk->get_where(array('id'=>$item['id']));
				$currentProductCat = $this->Kategori->get_where(array('id'=>$currentProductData[0]->kategori_id));
				$detail['id_produk'][] = $item['id'];
				$detail['kode_barang'][] = $item['id'];
				$detail['jumlah'][] = $item['total'];
				$detail['kode_produk'][] = $currentProductData[0]->kode_produk;
				$detail['satuan'][] = $currentProductData[0]->satuan;
				$detail['kategori'][] = $currentProductCat[0]->nama_kategori;
				$detail['harga_jual'][] = $currentProductData[0]->harga_jual;
				$detail['sub_total'][] = $currentProductData[0]->harga_jual*$item['total'];
				$detail['harga_pokok'][] = $currentProductData[0]->harga;
				$detail['total_pokok'][] = $currentProductData[0]->harga*$item['total'];

				// Min Stok
				$this->Produk->min_stok(@$item['id'],@$item['total']);
			}
		}

		$detail['nomor_faktur']=$nomor_faktur;
		$detail['tanggal_order']=date('Y-m-d');
		$detail['tanggal_terima']=NULL;
		$detail['id_admin']=2;
		$detail['uang_muka']=NULL;
		$detail['total']=$grandTotal;
		$detail['id_pelanggan']=$this->session->userdata('id');

		$detail = (object) $detail;

		$this->Penjualan->insert_online($detail);
		$this->Penjualan_detail->insert_online($detail);
		$this->Pembayaran->insert_online_cart($detail);
		// Redirect to Cart & Clean
		unset($_SESSION['cart']);

		if (EMAIL_MODE) {
			// Kirim Email ke Admin
			$to = SYSTEM_EMAIL;
			$subject = 'ACHALESVARA :: Pesanan!';
			$msg = "Pesanan baru Telah Masuk dan perlu di konfrimasi dengan Faktur: ".$nomor_faktur;
			$this->sendEmail($to,$subject,$msg);

			// Kirim Email ke Pelanggan
			$to = $this->session->userdata('data')->email;
			$subject = 'ACHALESVARA :: Pesanan Anda!';
			$msg = "Pesanan Anda telah masuk dengan Faktur: ".$nomor_faktur.". Kami akan segera mengecek dan konfirmasi pesanan anda, Tunggu Email Konfirmasi dari kami. atau hubungi: 081239390200.";
			$this->sendEmail($to,$subject,$msg);
		}


		$this->session->set_flashdata('sendorder', 'Pesanan Telah Masuk, Tunggu Email Konfirmasi dari kami.');
		return redirect(base_url('/checkout'),'refresh');
	}

	/* Halaman Lupa Password */
	public function lupa_password()
	{
		$data['fx'] = $this;
		$this->load->view('front/parts/header',$data);
		$this->load->view('front/lupa_password');
		$this->load->view('front/parts/footer');
	}
	/* Halaman Reset Password */
	public function reset_password()
	{
		if (@$_SESSION['user_reset'][0]->id == NULL) {
			return redirect(base_url('/'),'refresh');
		}
		$data['fx'] = $this;
		$this->load->view('front/parts/header',$data);
		$this->load->view('front/reset_password');
		$this->load->view('front/parts/footer');
	}

	/* Halaman Reset Password */
	public function reset_password_aksi()
	{
		$input = (object) $this->input->post(NULL);
		$data_user = @$_SESSION['user_reset'][0];
		unset($_SESSION['user_reset']);

		$this->Pelanggan->update_pass($input->pass,array('email'=>$data_user->email));
		
	}

	public function lupapass_aksi()
	{
		$input = (object) $this->input->post(NULL);
		// check data pemilik email
		$dt = $this->Pelanggan->get_where(array('email'=>$input->email));
		if (count($dt) > 0) {
			// Save Data Reset to Session
			$_SESSION['user_reset'] = $dt;


			$to = $input->email;
			$subject = 'ACHALESVARA :: Reset Password!';
			$msg = "Untuk Melakukan Reset Password silahkan melalui Link ini : ".base_url('reset-password');
			$this->sendEmail($to,$subject,$msg);
		}else{
			echo 'gagal';
		}
	}

}