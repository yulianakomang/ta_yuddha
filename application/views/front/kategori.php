<!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb_area bg-img" style="background-image: url(<?= base_url('asset/front') ?>/img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>Produk Kami</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Shop Grid Area Start ##### -->
    <section class="shop_grid_area section-padding-80">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <div class="shop_grid_product_area">
                        <div class="row">
                            <div class="col-12">
                                <div class="product-topbar d-flex align-items-center justify-content-between">
                                    <!-- Total Products -->
                                    <div class="total-products">
                                        <p><span><?= count($terbaru) ?></span> Produk</p>
                                    </div>
                                    <!-- Sorting -->
                                    <!-- <div class="product-sorting d-flex">
                                        <p>Pilih Kategori:</p>
                                        <form action="#" method="get">
                                            <select name="select" id="sortByselect">
                                                <option value="value">Highest Rated</option>
                                                <option value="value">Newest</option>
                                                <option value="value">Price: $$ - $</option>
                                                <option value="value">Price: $ - $$</option>
                                            </select>
                                            <input type="submit" class="d-none" value="">
                                        </form>
                                    </div> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            
                            <?php foreach ($terbaru as $item): ?>
                        <div class="col-12 col-sm-6 col-lg-4">
                        <!-- Single Product -->
                        <div class="single-product-wrapper">
                            <!-- Product Image -->
                            <div class="product-img">
                                <img src="<?= base_url('uploads/'.$item->gambar) ?>" alt="<?= $item->nama ?>">
                            </div>
                            <!-- Product Description -->
                            <div class="product-description">
                                <span><?= $fx->_kategori($item->id)->nama_kategori ?></span>
                                <a href="<?= base_url('detail/'.$item->kode_produk) ?>">
                                    <h6><?= $item->nama ?></h6>
                                </a>
                                <p class="product-price"><?= $fx->_rupiah($item->harga_jual) ?></p>

                                <!-- Hover Content -->
                                <div class="hover-content">
                                    <!-- Masukan Ke Keranjang -->
                                    <div class="add-to-cart-btn">
                                        <a href="<?= base_url('detail/'.$item->kode_produk) ?>" class="btn essence-btn cart-btn">Beli</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php endforeach ?>
                            


                        </div>
                    </div>
                    <!-- Pagination -->
                    <!-- <nav aria-label="navigation">
                        <ul class="pagination mt-50 mb-70">
                            <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">...</a></li>
                            <li class="page-item"><a class="page-link" href="#">21</a></li>
                            <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </nav> -->
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Shop Grid Area End ##### -->