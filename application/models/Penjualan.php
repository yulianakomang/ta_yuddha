<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Model {

	protected $table = 'tb_penjualan';

    public function removeDot($value='')
    {
        return str_replace('.', '', $value);
    }

	public function fetchAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->result();
    }
    public function countAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->num_rows();
    }

    public function getCondition($request='')
    {
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
             $this->db->where('tanggal_order >=',$request['tgl_start']);
            $this->db->where('tanggal_order <=',$request['tgl_end']);
         }
            $this->db->where('status','Lunas');
        return $this->db->get($this->table)->result();
    }


    public function get_where($data = '')
    {
        $this->db->where($data);
        return $this->db->get($this->table)->result();
    }

    public function getPenjualan($id_pelanggan='',$request='')
    {
        $this->db->where(array('id_pelanggan'=> $id_pelanggan));
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
            $this->db->where('tanggal_order >=',$request['tgl_start']);
            $this->db->where('tanggal_order <=',$request['tgl_end']);
        }
        return $this->db->get($this->table)->num_rows();
    }
    public function getPenjualanTotal($id_pelanggan='',$request='')
    {
        $this->db->select_sum('total');
        $this->db->where(array('id_pelanggan'=> $id_pelanggan));
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
            $this->db->where('tanggal_order >=',$request['tgl_start']);
            $this->db->where('tanggal_order <=',$request['tgl_end']);
        }
        
        return $this->db->get($this->table)->result()[0];
    }

    public function update_status($input='')
    {
         $data = array(
            'status'   => 'Lunas',
        );
        $this->db->where(array('nomor_faktur'=> $input->faktur));
        $this->db->update($this->table, $data);
    }

    public function update_status_online($input='')
    {
         $data = array(
            'status'   => 'Sedang di Proses',
            'bukti_tf' => $this->_uploadImage($input->nomor_faktur),
        );
        $this->db->where(array('nomor_faktur'=> $input->nomor_faktur));
        $this->db->update($this->table, $data);
    }

    public function approve($input='')
    {
         $data = array(
            'status'   => 'Lunas',
            'setujui'   => 1,
        );
        $this->db->where(array('nomor_faktur'=> $input->faktur));
        $this->db->update($this->table, $data);
    }



    public function insert($input = '')
    {
        $data = array(
            'nomor_faktur'         => $input->nomor_faktur,
            'id_admin'         => $input->id_admin,
            'id_pelanggan'         => $input->id_pelanggan,
            'tanggal_order'         => $input->tanggal_order,
            'tanggal_terima'         => $input->tanggal_terima,
            'uang_muka'         => $this->removeDot($input->uang_muka),
            'total'         => $this->removeDot($input->total),
            'status'         => '',
            'online'         => 0,
            'sisa' => ($this->removeDot($input->total) - $this->removeDot($input->uang_muka)),
            );
        $this->db->insert($this->table, $data);
    }

    public function insert_online($input = '',$mode='')
    {
        $data = array(
            'nomor_faktur'         => $input->nomor_faktur,
            'id_admin'         => $input->id_admin,
            'id_pelanggan'         => $input->id_pelanggan,
            'tanggal_order'         => $input->tanggal_order,
            'tanggal_terima'         => $input->tanggal_terima,
            'uang_muka'         => $this->removeDot($input->uang_muka),
            'total'         => $this->removeDot($input->total),
            'status'         => 'Sedang di Proses',
            'online'         => 1,
            'bukti_tf'         => $this->_uploadImage($input->nomor_faktur),
            'sisa' => @($this->removeDot($input->total) - $this->removeDot($input->uang_muka)),
            );
        $this->db->insert($this->table, $data);
        
    }

    public function update($input = '',$where)
    {
        
        $data = array(
            'nomor_faktur'         => $input->nomor_faktur,
            'id_admin'         => $input->id_admin,
            'id_pelanggan'         => $input->id_pelanggan,
            'tanggal_order'         => $input->tanggal_order,
            'tanggal_terima'         => $input->tanggal_terima,
            'uang_muka'         => $this->removeDot($input->uang_muka),
            'total'         => $this->removeDot($input->total),
            'status'         => '',
            'sisa' => ($this->removeDot($input->total) - $this->removeDot($input->uang_muka)),
        );
       
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function delete($where)
    {
        $this->db->delete($this->table,$where);
    }

    private function _uploadImage($faktur='')
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png';
        $config['file_name']            = 'Bukti_tf-'.$faktur;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('upload_bukti')) {
            return $this->upload->data("file_name");
        }
        
        return "default.jpg";
    }

}