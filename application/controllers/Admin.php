<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MyController.php';

class Admin extends MyController {

	/* Load Otomatis */
	public function __construct(){
		parent::__construct();
		/* Cek jika sudah login, jika tidak redirect ke halaman Login */
		if ($this->session->userdata('username') == null) {
			redirect('login');
		}

		/* Load Model yang dibutuhkan */
		$this->load->model('Pengguna');
		$this->load->model('Kategori');
		$this->load->model('Pelanggan');
		$this->load->model('Produk');
		$this->load->model('Penjualan');
		$this->load->model('Penjualan_detail');
		$this->load->model('Pembayaran');
	}

	/* Halaman Admin Dashboard */
	public function index($value='') {
		/* Halaman Admin Dashboard */
		$data['total_penjualan'] = $this->Penjualan->countAll();
		$data['total_pelanggan'] = $this->Pelanggan->countAll();
		$data['total_produk'] = $this->Produk->countAll();

		/* Menampilkan Desain per bagian */
		$this->load->view('admin/parts/header',$data);
		$this->load->view('admin/dashboard');
		$this->load->view('admin/parts/footer');
	}

	/* Produk List */
	public function produk()
	{

		$cat = @$_GET['kategori_id']; // mengambil id kategori
		if (!isset($cat) || $cat === '') { // jika tidak ada id kategori
			$data['list'] = $this->Produk->fetchAll(); // mengambil semua produk
		}else{ // jika isi
			$data['list'] = $this->Produk->get_where(array('kategori_id'=>$cat)); // mengambil produk sesuai kategori
		}

		$data['kategori_list'] = $this->Kategori->fetchAll(); // mangambil semua kategori
		/* Menampilkan Desain per bagian */
		$this->load->view('admin/parts/header');
		$this->load->view('admin/produk/list', $data);
		$this->load->view('admin/parts/footer');
	}


	public function produk_store()
	{
		$input = (object) $this->input->post(NULL);
		$this->Produk->insert($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah'));
		return redirect(base_url('admin/produk'),'refresh');
	}

	public function produk_update()
	{
		$input = (object) $this->input->post(NULL);
		$this->Produk->update($input,array('id'=>$input->id));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'ubah'));
		return redirect(base_url('admin/produk'),'refresh');
	}

	public function produk_hapus()
	{
		$id = $this->uri->segment(3);
		$this->Produk->delete(array('id'=>$id));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'hapus'));
		return redirect(base_url('admin/produk'),'refresh');
	}


	

	/* Pelanggan */
	public function pelanggan()
	{
		$data['list'] = $this->Pelanggan->fetchAll();
		$this->load->view('admin/parts/header');
		$this->load->view('admin/pelanggan/list', $data);
		$this->load->view('admin/parts/footer');
	}

	public function pelanggan_tambah()
	{
		$data['form_mode'] = 'add';
		$this->load->view('admin/parts/header');
		$this->load->view('admin/pelanggan/form', $data);
		$this->load->view('admin/parts/footer');
	}

	public function pelanggan_store()
	{
		$input = (object) $this->input->post(NULL);
		$this->Pelanggan->insert($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah'));
		return redirect(base_url('admin/pelanggan'),'refresh');
	}

	public function pelanggan_ubah()
	{
		$id = $this->uri->segment(3);
		$data['hasil'] = $this->Pelanggan->get_where(array('id'=>$id));
		$data['form_mode'] = 'edit';
		$this->load->view('admin/parts/header');
		$this->load->view('admin/pelanggan/form',$data);
		$this->load->view('admin/parts/footer');
	}

	public function pelanggan_update()
	{
		$input = (object) $this->input->post(NULL);
		$this->Pelanggan->update($input,array('id'=>$input->id));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'ubah'));
		return redirect(base_url('admin/pelanggan'),'refresh');
	}

	public function pelanggan_hapus()
	{
		$id = $this->uri->segment(3);
		$this->Pelanggan->delete(array('id'=>$id));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'hapus'));
		return redirect(base_url('admin/pelanggan'),'refresh');
	}

	/* Kategori */
	public function kategori()
	{
		$data['list'] = $this->Kategori->fetchAll();
		$this->load->view('admin/parts/header');
		$this->load->view('admin/kategori/list', $data);
		$this->load->view('admin/parts/footer');
	}

	public function kategori_tambah()
	{
		$data['form_mode'] = 'add';
		$this->load->view('admin/parts/header');
		$this->load->view('admin/kategori/form', $data);
		$this->load->view('admin/parts/footer');
	}

	public function kategori_store()
	{
		$input = (object) $this->input->post(NULL);
		$this->Kategori->insert($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah'));
		return redirect(base_url('admin/kategori'),'refresh');
	}

	public function kategori_ubah()
	{
		$id = $this->uri->segment(3);
		$data['hasil'] = $this->Kategori->get_where(array('id'=>$id));
		$data['form_mode'] = 'edit';
		$this->load->view('admin/parts/header');
		$this->load->view('admin/kategori/form',$data);
		$this->load->view('admin/parts/footer');
	}

	public function kategori_update()
	{
		$input = (object) $this->input->post(NULL);
		$this->Kategori->update($input,array('id'=>$input->id));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'ubah'));
		return redirect(base_url('admin/kategori'),'refresh');
	}

	public function kategori_hapus()
	{
		$id = $this->uri->segment(3);
		$this->Kategori->delete(array('id'=>$id));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'hapus'));
		return redirect(base_url('admin/kategori'),'refresh');
	}

	/* Pengguna */
	public function pengguna()
	{
		$data['list'] = $this->Pengguna->fetchAll();
		$this->load->view('admin/parts/header');
		$this->load->view('admin/pengguna/list', $data);
		$this->load->view('admin/parts/footer');
	}

	public function pengguna_tambah()
	{
		$data['form_mode'] = 'add';
		$this->load->view('admin/parts/header');
		$this->load->view('admin/pengguna/form', $data);
		$this->load->view('admin/parts/footer');
	}

	public function pengguna_store()
	{
		$input = (object) $this->input->post(NULL);
		$this->Pengguna->insert($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah'));
		return redirect(base_url('admin/pengguna'),'refresh');
	}

	public function pengguna_ubah()
	{
		$id = $this->uri->segment(3);
		$data['hasil'] = $this->Pengguna->get_where(array('id'=>$id));
		$data['form_mode'] = 'edit';
		$this->load->view('admin/parts/header');
		$this->load->view('admin/pengguna/form',$data);
		$this->load->view('admin/parts/footer');
	}

	public function pengguna_update()
	{
		$input = (object) $this->input->post(NULL);
		$this->Pengguna->update($input,array('id'=>$input->id));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'ubah'));
		return redirect(base_url('admin/pengguna'),'refresh');
	}

	public function pengguna_hapus()
	{
		$id = $this->uri->segment(3);
		$this->Pengguna->delete(array('id'=>$id));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'hapus'));
		return redirect(base_url('admin/pengguna'),'refresh');
	}

	/* Penjualan */
	public function penjualan($id = null){
		$data['list'] = $this->Penjualan->get_where(array('online'=>0));
		$data['admin_list'] = $this->Pengguna->fetchAll();
		$data['pelanggan_list'] = $this->Pelanggan->fetchAll();
		$this->load->view('admin/parts/header');
		$this->load->view('admin/penjualan/list', $data);
		$this->load->view('admin/parts/footer');
	}
	public function penjualan_tambah()
	{
		$data['form_mode'] = 'add';
		$data['admin_list'] = $this->Pengguna->fetchAll();
		$data['pelanggan_list'] = $this->Pelanggan->fetchAll();
		$data['produk_list'] = $this->Produk->fetchAll();
		$data['kategori_list'] = $this->Kategori->fetchAll();
		$this->load->view('admin/parts/header');
		$this->load->view('admin/penjualan/form', $data);
		$this->load->view('admin/parts/footer');
	}

	public function penjualan_store()
	{
		$input = (object) $this->input->post(NULL);
		$this->Penjualan->insert($input);
		$this->Penjualan_detail->insert($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah'));
		return redirect(base_url('admin/penjualan'),'refresh');
	}

	public function penjualan_ubah()
	{
		$id = $this->uri->segment(3);
		$data['hasil'] = $this->Penjualan->get_where(array('id'=>$id));
		$data['form_mode'] = 'edit';
		$data['produk_list'] = $this->Produk->fetchAll();
		$data['kategori_list'] = $this->Kategori->fetchAll();
		$data['admin_list'] = $this->Pengguna->fetchAll();
		$data['pelanggan_list'] = $this->Pelanggan->fetchAll();
		$data['detail_list']= $this->Penjualan_detail->get_where(array('nomor_faktur'=>$data['hasil'][0]->nomor_faktur));
		
		$this->load->view('admin/parts/header');
		$this->load->view('admin/penjualan/form',$data);
		$this->load->view('admin/parts/footer');
	}

	public function penjualan_update()
	{
		$input = (object) $this->input->post(NULL);
		$this->Penjualan->update($input,array('id'=>$input->id));
		$this->Penjualan_detail->update($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'ubah'));
		if ($input->hal == 'order') {
			return redirect(base_url('admin/order'),'refresh');
		}
		return redirect(base_url('admin/penjualan'),'refresh');
	}

	public function penjualan_hapus()
	{
		$id = $this->uri->segment(3);
		$faktur = $this->uri->segment(4);
		$this->Penjualan->delete(array('id'=>$id));
		$this->Penjualan_detail->delete(array('nomor_faktur'=>$faktur));
		$this->Pembayaran->delete(array('nomor_faktur'=>$faktur));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'hapus'));
		if (@$_GET['hal'] == 'order') {
			return redirect(base_url('admin/order'),'refresh');
		}
		return redirect(base_url('admin/penjualan'),'refresh');
	}

	public function order()
	{
		$data['list'] = $this->Penjualan->get_where(array('online'=>1));
		$data['admin_list'] = $this->Pengguna->fetchAll();
		$data['pelanggan_list'] = $this->Pelanggan->fetchAll();
		$this->load->view('admin/parts/header');
		$this->load->view('admin/order/list', $data);
		$this->load->view('admin/parts/footer');
	}

	

	public function pembayaran_store()
	{
		$input = (object) $this->input->post(NULL);
		$this->Pembayaran->insert($input);
		// ubah status
		$this->Penjualan->update_status($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah-pembayaran'));
		return redirect(base_url('admin/penjualan'),'refresh');
	}

	public function user_pembayaran_store()
	{
		$input = (object) $this->input->post(NULL);
		$this->Pembayaran->insert_online($input);
		// ubah status
		$this->Penjualan->update_status_online($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah-pembayaran'));
		return redirect(base_url('admin/user_barang_list'),'refresh');
	}

	public function user_pembayaran_approve()
	{
		$input = (object) $this->input->post(NULL);

		$data_pesanan = $this->Penjualan->get_where(array('nomor_faktur'=>$input->faktur));
		$data_pelanggan = $this->Pelanggan->get_where(array('id'=>$data_pesanan[0]->id_pelanggan));

		$this->Penjualan->approve($input);

		if (EMAIL_MODE) {
			// Kirim Email ke Admin
			$to = SYSTEM_EMAIL;
			$subject = 'ACHALESVARA :: Pesanan diKonfirmasi!';
			$msg = "Pesanan telah di Konfirmasi dengan faktur: ".$input->faktur;
			$this->sendEmail($to,$subject,$msg);

			// Kirim Email ke Pelanggan
			$to = @$data_pelanggan[0]->email;
			$subject = 'ACHALESVARA :: Pesanan Anda telah di Konfirmasi!';
			$msg = "Pesanan Anda telah dikonfirmasi dengan Faktur: ".$input->faktur.". Kami akan segera mengirim pesanan anda, Tunggu pesanan anda datang. atau hubungi: 081239390200.";
			$this->sendEmail($to,$subject,$msg);

		}


		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'konfir-pembayaran'));
		return redirect(base_url('admin/order'),'refresh');
	}

	public function detail_pembayaran($value='')
	{
		$faktur = $this->uri->segment(3);
		$data['penjualan'] = $this->Penjualan->get_where(array('nomor_faktur'=>$faktur));
		$data['list'] = $this->Penjualan_detail->get_where(array('nomor_faktur'=>$faktur));
		$data['produk_list'] = $this->Produk->fetchAll();
		$data['kategori_list'] = $this->Kategori->fetchAll();
		$data['pembayaran'] = $this->Pembayaran->get_where(array('nomor_faktur'=>$faktur));
		$data['pelanggan_list'] = $this->Pelanggan->fetchAll();
		$this->load->view('admin/parts/header');
		$this->load->view('admin/penjualan/detail', $data);
		$this->load->view('admin/parts/footer');
	}


	public function laporan_data_barang()
	{
		$cat = @$_GET['kategori_id'];
		if (!isset($cat) || $cat === '') {
			$data['list'] = $this->Produk->fetchAll();
		}else{
			$data['list'] = $this->Produk->get_where(array('kategori_id'=>$cat));
		}

		$data['kategori_list'] = $this->Kategori->fetchAll();
		$data['func'] = $this->Produk;

		$this->load->view('admin/parts/header');
		$this->load->view('admin/laporan/data_barang', $data);
		$this->load->view('admin/parts/footer');
		

	}

	public function laporan_penjualan()
	{
		$data['list'] = $this->Penjualan->getCondition(@$_GET);
		$data['admin_list'] = $this->Pengguna->fetchAll();
		$data['pelanggan_list'] = $this->Pelanggan->fetchAll();
		$data['func'] = $this->Pembayaran;
		if (@$_GET['cetak']) {
			$this->load->view('admin/cetak/penjualan',$data);
		}else{
			$this->load->view('admin/parts/header');
			$this->load->view('admin/laporan/penjualan', $data);
			$this->load->view('admin/parts/footer');
		}
	}

	public function laporan_pelanggan()
	{
		$data['list'] = $this->Pelanggan->get_laporan_pelanggan($this->input->get('tgl_start'), $this->input->get('tgl_end'));
		$data['func'] = $this->Penjualan;

		// var_dump($data);

		if (@$_GET['cetak']) {
			$this->load->view('admin/cetak/pelanggan',$data);
		}else{
			$this->load->view('admin/parts/header');
			$this->load->view('admin/laporan/pelanggan', $data);
			$this->load->view('admin/parts/footer');
		}

	}

	/* -- User Dashboard -- */

	/* Penjualan */
	public function user_barang_list($id = null){
		$data['list'] = $this->Penjualan->get_where(array('online'=>1,'id_pelanggan'=>$this->session->userdata('id')));
		$data['admin_list'] = $this->Pengguna->fetchAll();
		$data['pelanggan_list'] = $this->Pelanggan->fetchAll();
		$this->load->view('admin/parts/header');
		$this->load->view('admin/penjualan/user_list', $data);
		$this->load->view('admin/parts/footer');
	}
	public function user_barang_tambah()
	{
		$data['form_mode'] = 'add';
		$data['admin_list'] = $this->Pengguna->fetchAll();
		$data['pelanggan_list'] = $this->Pelanggan->fetchAll();
		$data['produk_list'] = $this->Produk->fetchAll();
		$data['kategori_list'] = $this->Kategori->fetchAll();
		$this->load->view('admin/parts/header');
		$this->load->view('admin/penjualan/user_form', $data);
		$this->load->view('admin/parts/footer');
	}

	public function user_barang_store()
	{
		$input = (object) $this->input->post(NULL);
		$this->Penjualan->insert_online($input);
		$this->Penjualan_detail->insert($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'tambah'));
		return redirect(base_url('admin/user_barang_list'),'refresh');
	}

	public function user_barang_ubah()
	{
		$id = $this->uri->segment(3);
		$data['hasil'] = $this->Penjualan->get_where(array('id'=>$id));
		$data['form_mode'] = 'edit';
		$data['produk_list'] = $this->Produk->fetchAll();
		$data['kategori_list'] = $this->Kategori->fetchAll();
		$data['admin_list'] = $this->Pengguna->fetchAll();
		$data['pelanggan_list'] = $this->Pelanggan->fetchAll();
		$data['detail_list']= $this->Penjualan_detail->get_where(array('nomor_faktur'=>$data['hasil'][0]->nomor_faktur));
		
		
		$this->load->view('admin/parts/header');
		$this->load->view('admin/penjualan/user_form',$data);
		$this->load->view('admin/parts/footer');
	}

	public function user_barang_update()
	{
		$input = (object) $this->input->post(NULL);
		$this->Penjualan->update($input,array('id'=>$input->id));
		$this->Penjualan_detail->update($input);
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'ubah'));
		return redirect(base_url('admin/user_barang_list'),'refresh');
	}

	public function user_barang_hapus()
	{
		$id = $this->uri->segment(3);
		$faktur = $this->uri->segment(4);
		$this->Penjualan->delete(array('id'=>$id));
		$this->Penjualan_detail->delete(array('nomor_faktur'=>$faktur));
		$this->Pembayaran->delete(array('nomor_faktur'=>$faktur));
		$this->session->set_flashdata(array('sweetalert'=>'success','opsi'=>'hapus'));
		return redirect(base_url('admin/user_barang_list'),'refresh');
	}






}
