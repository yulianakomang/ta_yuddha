-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 05, 2020 at 03:01 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta_yuddha`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(128) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `username` char(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `level` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `nama`, `alamat`, `username`, `password`, `no_telp`, `level`) VALUES
(1, 'I Wayan Admin', 'Jl. Sistem, No. 500', 'owner', '81dc9bdb52d04dc20036dbd8313ed055', '081299881988', 1),
(2, 'I Wayan User', 'Jl. Server, No. 404', 'admin', '81dc9bdb52d04dc20036dbd8313ed055', '08888888888888', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_penjualan`
--

CREATE TABLE `tb_detail_penjualan` (
  `id` int(11) UNSIGNED NOT NULL,
  `nomor_faktur` char(16) NOT NULL DEFAULT '',
  `id_produk` int(11) UNSIGNED DEFAULT NULL,
  `jumlah` int(11) NOT NULL DEFAULT '0',
  `subtotal` decimal(12,2) DEFAULT NULL,
  `total_pokok` decimal(12,2) DEFAULT NULL,
  `tgl_order` date DEFAULT NULL,
  `satuan` varchar(50) DEFAULT NULL,
  `harga_jual` decimal(12,2) NOT NULL,
  `harga_pokok` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama_kategori` varchar(128) NOT NULL DEFAULT '',
  `deskripsi` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `nama_kategori`, `deskripsi`) VALUES
(7, 'Obat', 'Obat'),
(8, 'Vitamin', 'Vitamin'),
(9, 'Desinfektan', 'Desinfektan'),
(10, 'Antibiotik', 'Antibiotik');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `id` int(11) UNSIGNED NOT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(128) DEFAULT '',
  `no_telp` char(14) DEFAULT NULL,
  `alamat` char(128) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `ktp` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`id`, `kode`, `nama`, `no_telp`, `alamat`, `username`, `password`, `email`, `ktp`) VALUES
(8, 'PLG-20200709101605', 'Gede Yuda Guna', '081299881988', 'Gianyar', 'yuddha', '4c91474757addc737b184bdb50977bce', 'gedeyudaguna@gmail.com', '5105041011970001'),
(9, 'PLG-20200714010829', 'Sunarta', '081299881988', 'Jl. Sistem, No. 500', 'sunarta', '81dc9bdb52d04dc20036dbd8313ed055', 'yulianakomanggede@gmail.com', '123456789');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `id` int(11) UNSIGNED NOT NULL,
  `nomor_faktur` char(16) NOT NULL,
  `tanggal_pembayaran` date DEFAULT NULL,
  `sisa_pembayaran` decimal(10,2) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_penjualan`
--

CREATE TABLE `tb_penjualan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor_faktur` char(16) NOT NULL DEFAULT '',
  `id_admin` int(10) UNSIGNED NOT NULL,
  `id_pelanggan` int(11) UNSIGNED DEFAULT NULL,
  `tanggal_order` date DEFAULT NULL,
  `tanggal_terima` date DEFAULT NULL,
  `uang_muka` decimal(10,2) DEFAULT NULL,
  `total` decimal(12,2) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `sisa` decimal(12,2) DEFAULT NULL,
  `online` int(100) NOT NULL,
  `bukti_tf` varchar(100) NOT NULL,
  `setujui` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id` int(11) UNSIGNED NOT NULL,
  `kode_produk` varchar(100) DEFAULT NULL,
  `nama` varchar(128) DEFAULT NULL,
  `harga` decimal(10,2) NOT NULL DEFAULT '0.00',
  `gambar` varchar(128) DEFAULT NULL,
  `satuan` char(10) DEFAULT NULL,
  `harga_jual` decimal(10,2) NOT NULL DEFAULT '0.00',
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `keterangan` text,
  `stok` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`id`, `kode_produk`, `nama`, `harga`, `gambar`, `satuan`, `harga_jual`, `kategori_id`, `keterangan`, `stok`) VALUES
(18, 'BRG-20200709125700', 'Vita Chicks', '2000.00', 'BRG-20200709125700.JPG', 'Sachet', '3000.00', 7, 'Vita Chick merupakan vitamin untuk anak ayam dan ayam dewasa, berupa serbuk yang dapat dilarutkan dengan air berwarna coklat muda yang mengandung banyak multivitamin yang dikombinasikan dengan growth promoter antibiotic (Probiotik pemacu pertumbuhan) agar pertumbuhan anak ayam menjadi maksimal.', 9),
(19, 'BRG-20200709023625', 'Antiseptik DESTAN 1lt', '79000.00', 'BRG-20200709023625.JPG', 'Botol', '82000.00', 9, 'DESTAN, Desinfektan dan Antiseptik, Ampuh, aman, spektrum luas  Larutan mengandung Benzalkonium chloride 10 %  Indikasi DESTAN* digunakan sebagai destnfektan dan antiseptik pada petemakan, laboratorium dan klinik hewan.  Dosis dan cara pemakaian Dosis Indikasi 60 ml /10 liter untuk luas area 40 50 m2 Desinfeksi kandang, tempat pakan dan minum, mesin tetas, sepatu kandang. 5ml/10 liter Desinfeksi air minum (unggas). 3 ml/10liter Desinfeksi air minum (babi). 2 ml/10 liter Desinfeksi air minum (sapi, kambing dan kuda). Dipping puting (* 30 detik) 10 ml/10 liter Desinfeksi alat-alat operasi (dokter hewan praktek).', 8),
(20, 'BRG-20200709023857', 'Neo Meditril', '120000.00', 'BRG-20200709023857.JPG', 'Botol', '124000.00', 9, 'NEO MEDITRIL adalah obat berbentuk larutan yang mengandung Enrofloxacin, suatu antibakteri derivat Fluoroquinolon yang bekerja luas membasmi bakteri. Daya kerjanya adalah berdasarkan hambatan terhadap enzim DNA gyrase yang diperlukan untuk pembelahan inti sel bakteri. NEO MEDITRIL efektif membasmi Mycoplasma sp., Escherichia coli, Haemophilus paragallinarum dan Pasteurella multocida', 1),
(21, 'BRG-20200709024439', 'Probio 7 ', '25000.00', 'BRG-20200709024439.JPG', 'Botol', '29000.00', 7, 'Probiotik dengan kandungan 7 mikroorganisme yang berfungsi sebagai bahan aktip fermentasi pakan ternak', 18),
(22, 'BRG-20200709025237', 'Koleridin', '4000.00', 'BRG-20200709025237.JPG', 'Sachet', '6000.00', 7, 'KOLERIDIN merupakan antibiotik yang mengandung kombinasi dua antibiotik dan vitamin K3. Kombinasi tersebut sangat efektif untuk menekan pertumbuhan bakteri sehingga dapat mempercepat proses penyembuhan penyakit. Indikasi: - Kolera (berak hijau) - CRD (ngorok) - Pullorum (berak kapur) Dosis: 0,2 gram tiap kg berat badan atau 1 gram tiap 1 liter air minum diberikan selama 4-5 hari berturut-turut', 20),
(23, 'BRG-20200709025903', 'TRIMEZYN S 100 gram', '23000.00', 'BRG-20200709025903.JPG', 'Sachet', '26000.00', 10, 'Obat ngorok, batuk, berak ijo, berak putih', 20),
(24, 'BRG-20200709030002', 'Biosan TP Oral', '37000.00', 'BRG-20200709030002.JPG', 'Botol', '40000.00', 8, 'BIOSAN TP ORAL merupakan gabungan energi siap pakai (ATP), mineral dan vitamin yg mampu menjaga stamina & meningkatkan daya tahan tubuh ayam, unggas dan burung pasca kelalahan dan sakit  KOMPOSISI Tiap ml mengandung : Adenosine triphosphat 1,1 mg Vitamin B12 0,5 mg Mg aspartat 15,0 mg K aspartat 10,0 mg Na selenit 1,0 mg Glycine 51,0 mg  INDIKASI  - Meningkatkan kondisi dan daya tahan tubuh ayam. - Menjaga dan mengembalikan stamina tubuh ayam yang kekelahan akibat transportasi, sedang sakit atau baru sembuh dari sakit.  DOSIS DAN CARA PEMAKAIAN - Larutkan 1ml Biosan TP Oral per 2 liter air minum ayam. - Berikan selama 3-5 hari berturut-turut. ', 15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_detail_penjualan`
--
ALTER TABLE `tb_detail_penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD PRIMARY KEY (`nomor_faktur`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_detail_penjualan`
--
ALTER TABLE `tb_detail_penjualan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
