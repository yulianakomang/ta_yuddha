<!-- ##### Welcome Area Start ##### -->
    <section class="welcome_area bg-img background-overlay" style="background-image: url(<?= base_url('asset/front') ?>/img/bg-img/bg-1.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="hero-content" style="margin-top: -150px">
                        <h6>Menyediakan Produk Kualitas Tinggi</h6>
                        <h2>Tunggu apa lagi?</h2>
                        <a href="<?= base_url('kategori')?>" class="btn essence-btn">Lihat Produk</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Welcome Area End ##### -->

    <!-- ##### New Arrivals Area Start ##### -->
    <section class="new_arrivals_area section-padding-80 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>Produk Populer</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="popular-products-slides owl-carousel">

                    <?php foreach ($popular as $item): ?>
                        
                        <!-- Single Product -->
                        <div class="single-product-wrapper">
                            <!-- Product Image -->
                            <div class="product-img">
                                <img src="<?= base_url('uploads/'.$item->gambar) ?>" alt="<?= $item->nama ?>">
                            </div>
                            <!-- Product Description -->
                            <div class="product-description">
                                <span><?= $fx->_kategori($item->id)->nama_kategori ?></span>
                                <a href="<?= base_url('detail/'.$item->kode_produk) ?>">
                                    <h6><?= $item->nama ?></h6>
                                </a>
                                <p class="product-price"><?= $fx->_rupiah($item->harga_jual) ?></p>

                                <!-- Hover Content -->
                                <div class="hover-content">
                                    <!-- Masukan Ke Keranjang -->
                                    <div class="add-to-cart-btn">
                                        <a href="<?= base_url('detail/'.$item->kode_produk) ?>" class="btn essence-btn cart-btn">Beli</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endforeach ?>

                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### New Arrivals Area End ##### -->
    <!-- ##### New Arrivals Area Start ##### -->
    <section class="new_arrivals_area section-padding-80 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>Produk Baru</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="popular-products-slides owl-carousel">

                        
                    <?php foreach ($terbaru as $item): ?>
                        
                        <!-- Single Product -->
                        <div class="single-product-wrapper">
                            <!-- Product Image -->
                            <div class="product-img">
                                <img src="<?= base_url('uploads/'.$item->gambar) ?>" alt="<?= $item->nama ?>">
                            </div>
                            <!-- Product Description -->
                            <div class="product-description">
                                <span><?= $fx->_kategori($item->id)->nama_kategori ?></span>
                                <a href="<?= base_url('detail/'.$item->kode_produk) ?>">
                                    <h6><?= $item->nama ?></h6>
                                </a>
                                <p class="product-price"><?= $fx->_rupiah($item->harga_jual) ?></p>

                                <!-- Hover Content -->
                                <div class="hover-content">
                                    <!-- Masukan Ke Keranjang -->
                                    <div class="add-to-cart-btn">
                                        <a href="<?= base_url('detail/'.$item->kode_produk) ?>" class="btn essence-btn cart-btn">Beli</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endforeach ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### New Arrivals Area End ##### -->